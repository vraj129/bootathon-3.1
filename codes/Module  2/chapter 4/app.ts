function calc()
{
    var input:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");   //getting id
    var table:HTMLTableElement = <HTMLTableElement>document.getElementById("table1"); //getting id
    var num:number;
    var count:number = 1;
    if(input.value == "") // checking if number is empty
    {
        alert("Enter The Number First !!!");
        return;
    }

    num = parseInt(input.value); // parsing the number

    if(isNaN(num)) // checking if it is number
    {
        alert("Enter Only Numbers !!!");
        return;
    }
    if(num<0) // checking if number is less than 0
    {
        alert("Enter Number Greater than 0");
        return;
    }
    if(num>1000) // checking if number is greater than 1000
    {
        alert("Enter Number Less Than 1000 otherwise Your PC will Hang !!!");
        return;
    }
    while(table.rows.length >1) // deleting rows by after the first row
    {
        table.deleteRow(1);
    }

    for(count = 1 ; count <=num; count++) // runnin the loop till the number value
    {
        var row:HTMLTableRowElement = table.insertRow();   //creating row element
        var cell:HTMLTableDataCellElement = row.insertCell(); // creating cell element
        
        var text:HTMLInputElement = document.createElement("input"); // creating input element
        text.type = "text";                                         // defining it to text
        text.readOnly=true;                                        // defining it to readonly
        text.style.border="0"                                        //styling it
        text.style.textAlign = "center";
        text.style.backgroundColor="rgb(35, 37, 37)";
        text.style.color = "rgb(224, 217, 217)";
        text.value = num.toString();        // assigning the value
        cell.appendChild(text);             // appending the text

        var cell:HTMLTableDataCellElement = row.insertCell();

        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.readOnly=true;
        text.style.border="0"
        text.style.textAlign = "center";
        text.style.backgroundColor="rgb(35, 37, 37)";
        text.style.color = "rgb(224, 217, 217)";
        text.value = "*"; 
        cell.appendChild(text);

        var cell:HTMLTableDataCellElement = row.insertCell();

        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.readOnly=true;
        text.style.border="0"
        text.style.textAlign = "center";
        text.style.backgroundColor="rgb(35, 37, 37)";
        text.style.color = "rgb(224, 217, 217)";
        text.value = count.toString(); 
        cell.appendChild(text);

        var cell:HTMLTableDataCellElement = row.insertCell();

        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.readOnly=true;
        text.style.border="0"
        text.style.textAlign = "center";
        text.style.backgroundColor="rgb(35, 37, 37)";
        text.style.color = "rgb(224, 217, 217)";
        text.value = "="; 
        cell.appendChild(text);

        var cell:HTMLTableDataCellElement = row.insertCell();

        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.readOnly=true;
        text.style.border="0"
        text.style.textAlign = "center";
        text.style.backgroundColor="rgb(35, 37, 37)";
        text.style.color = "rgb(224, 217, 217)";
        text.value = (num*count).toString(); 
        cell.appendChild(text);


    }
}