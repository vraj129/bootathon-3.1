var ax:HTMLInputElement = <HTMLInputElement>document.getElementById("ax");    //getting id
var ay:HTMLInputElement = <HTMLInputElement>document.getElementById("ay");    //getting id
var bx:HTMLInputElement = <HTMLInputElement>document.getElementById("bx");    //getting id
var by:HTMLInputElement = <HTMLInputElement>document.getElementById("by");    //getting id
var cx:HTMLInputElement = <HTMLInputElement>document.getElementById("cx");    //getting id
var cy:HTMLInputElement = <HTMLInputElement>document.getElementById("cy");    //getting id
var xx:HTMLInputElement = <HTMLInputElement>document.getElementById("xx");    //getting id
var yy:HTMLInputElement = <HTMLInputElement>document.getElementById("yy");    //getting id
var res:HTMLInputElement = <HTMLInputElement>document.getElementById("res");  //getting id


function calc()
{
    if(ax.value == "" || ay.value == "" || bx.value == "" || by.value == "" || cx.value == "" || cy.value == "" || xx.value == "" || yy.value == "") // checking if value is not empty
    {
        alert("Enter The Value of All Fields !!!");
        return;
    }
    var ax1 = parseFloat(ax.value); //parsing to Float
    var ay1 = parseFloat(ay.value); //parsing to Float
    var bx1 = parseFloat(bx.value); //parsing to Float
    var by1 = parseFloat(by.value); //parsing to Float
    var cx1 = parseFloat(cx.value); //parsing to Float
    var cy1 = parseFloat(cy.value); //parsing to Float
    var xx1 = parseFloat(xx.value); //parsing to Float
    var yy1 = parseFloat(yy.value); //parsing to Float
    res.value="";
    if(isNaN(ax1) || isNaN(ay1) || isNaN(bx1) || isNaN(by1) || isNaN(cx1) || isNaN(cy1) || isNaN(xx1) || isNaN(yy1)) //check if it is number
    {
        var text = "Enter Only Number !!!"
        alert(text);
        return;
    }
    else
    {
        var res1:number;
        var res2:number;
        var res3:number;
        var res4:number;
        var str:string;
        var str1:string;
        str = xx.value;
        str1 = yy.value;
        res1 = cal(ax1,ay1,bx1,by1,cx1,cy1); //calculating area 
        res2 = cal(ax1,ay1,bx1,by1,xx1,yy1); //calculating area 
        res3 = cal(bx1,by1,cx1,cy1,xx1,yy1); //calculating area 
        res4 = cal(ax1,ay1,cx1,cy1,xx1,yy1); //calculating area 
        res.value = res1.toString();
        var num11:number;
        num11 = res2 + res3 + res4; // adding the area 
        if(res1 == num11) // checking if the area is equl to the calculated area
        {
            document.getElementById("display").innerHTML="<b>The Co ordinates ("+str+","+str1+") are inside the triangle<b>"; //giving the appropriate answer 
        }
        else
        {
            document.getElementById("display").innerHTML="<b>The Co ordinates ("+str+","+str1+") are not inside the triangle<b>"; //giving the appropriate answer
        }
        
    }

}
 
function cal( ax2, ay2, bx2, by2, cx2,cy2)
{
    return Math.abs((ax2*(by2-cy2) + bx2*(cy2-ay2) + cx2*(ay2-by2))/2);   //formula to calculate area
}