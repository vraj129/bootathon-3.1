var ax = document.getElementById("ax");
var ay = document.getElementById("ay");
var bx = document.getElementById("bx");
var by = document.getElementById("by");
var cx = document.getElementById("cx");
var cy = document.getElementById("cy");
var xx = document.getElementById("xx");
var yy = document.getElementById("yy");
var res = document.getElementById("res");
function calc() {
    if (ax.value == "" || ay.value == "" || bx.value == "" || by.value == "" || cx.value == "" || cy.value == "" || xx.value == "" || yy.value == "") {
        alert("Enter The Value of All Fields !!!");
        return;
    }
    var ax1 = parseFloat(ax.value);
    var ay1 = parseFloat(ay.value);
    var bx1 = parseFloat(bx.value);
    var by1 = parseFloat(by.value);
    var cx1 = parseFloat(cx.value);
    var cy1 = parseFloat(cy.value);
    var xx1 = parseFloat(xx.value);
    var yy1 = parseFloat(yy.value);
    res.value = "";
    if (isNaN(ax1) || isNaN(ay1) || isNaN(bx1) || isNaN(by1) || isNaN(cx1) || isNaN(cy1) || isNaN(xx1) || isNaN(yy1)) {
        var text = "Enter Only Number !!!";
        alert(text);
        return;
    }
    else {
        var res1;
        var res2;
        var res3;
        var res4;
        var str;
        var str1;
        str = xx.value;
        str1 = yy.value;
        res1 = cal(ax1, ay1, bx1, by1, cx1, cy1);
        res2 = cal(ax1, ay1, bx1, by1, xx1, yy1);
        res3 = cal(bx1, by1, cx1, cy1, xx1, yy1);
        res4 = cal(ax1, ay1, cx1, cy1, xx1, yy1);
        res.value = res1.toString();
        var num11;
        num11 = res2 + res3 + res4;
        if (res1 == num11) {
            document.getElementById("display").innerHTML = "<b>The Co ordinates (" + str + "," + str1 + ") are inside the triangle<b>";
        }
        else {
            document.getElementById("display").innerHTML = "<b>The Co ordinates (" + str + "," + str1 + ") are not inside the triangle<b>";
        }
    }
}
function cal(ax2, ay2, bx2, by2, cx2, cy2) {
    return Math.abs((ax2 * (by2 - cy2) + bx2 * (cy2 - ay2) + cx2 * (ay2 - by2)) / 2);
}
//# sourceMappingURL=app.js.map